## Update Changelog - 2021-03-01 (Ep. 10.3 supplementary update)

### Changelog for Players

#### Changed
- **WoE**: Disabled elemental slotted katars in pre-trans WoE. [37169dd340, e5b3d610b7]
- **WoE**: Disabled some other trans weapons in pre-trans WoE. [Click here](https://gitlab.com/originsro/originsro/-/blob/master/changelogs/attachments/pre-trans-woe-item-blacklist.md) for an up to date list of the blacklisted items. [c740568b95, 0b91c2455a]
- **Maps**: Disabled Dead Branches on jupe_core, thana_step and thana_boss. [df11663d97, ccf4fe658a, fd19608a47, 3dd822d889]

#### Fixed
- **Skills**: Fixed Happy Break not triggering on /doridori anymore. [3056acfe62, 5d176c05d1]
- **Homunculi**: Fixed too fast Homunculus SP regeneration. [89a1767e8e, ab80df3877, cc5b454ca1, 512bd9e992]

### Removed
- **Event**: Removed Christmas Event.

> This update consists of 15 commits.
