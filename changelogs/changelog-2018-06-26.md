# Update Changelog - 2018-06-26 (part of Ep. 9.2.1)

### Notes

- **The game time is now displayed in UTC**, to avoid confusion. This change is reflected in the CP and website as well. **The WoE time in your local timezone will not change**, this just modifies the way the time is displayed.
- **The game has been migrated to a new, faster, server** with system specs more suitable for the current population, and a more recent and up to date operating system. Other, less critical, parts or OriginsRO are still on the old server (including the website and other web services), and will be migrated to separate machines in the near future.

## Changelog for Players

### Added

- **Items**:     Added 10 new costumes: [1412c2c018, f53d40f074, C:f75e09152f]
  - Headset Costume
  - Ear Muffs Costume
  - Hair Band Costume
  - Cap Costume
  - Coronet Costume
  - Sea Otter Hat Costume
  - Glasses Costume
  - Goblin Mask Costume
  - Munak Hat Costume
  - Unicorn Horn Costume
- **Commands**:  Added a new `@daynight` command, to disable the day/night visual effect. The command can be automated through `NPC:AutoCommands` as usual. [334649a373, 48ce5e6cbd, 11af6e7e6e]
- **UI**:        The storage window has been updated to show the type of storage currently in use (Storage, Guild Storage, Master Storage). [4fde3324c4, 23758ce0dd, 923e168db1]
- **NPCs**:      Added Guild Storage to all the Kafras that already have Account and Master Storage. [566e5ceb87] OriginsRO#676
- **Commands**:  Added the `@event` command, as a user-friendly alias for the event `NPC:Warper` service. [0960921d16] OriginsRO#760

### Changed

- **Skills**:    Changed Ankle Snare behavior in WoE to prevent further steps once triggered. [85b096cc8d] Related to OriginsRO#663
- **Items**:     Consolidated the names of items related to Sea Otters (rather than Sea-Otters). [2f96647862]
- **Events**:    Some monsters from crowded maps are removed from the Monster of the Day selection. [7509114dc3]
- **Items**:     Consumable items can be now used when the storage is open. [408982006d]
- **Skills**:    Some skill/item failure messages have been updated to be more similar to the official ones. [b20e9cbb32]
- **Mobs**:      The MVP tombstones will now appear after a short delay, to avoid covering any important items on ground. [402a20fbba]
- **Misc**:      Improved handling of international text. [c84950989e]
- **Skills**:    Improved handling of guild skill cooldowns, to allow them to continue even when the character is logged out. [57ba23012d]
- **Skills**:    Updated Blessing behavior to be closer to the official, for more fine-grained control. When the target is under Curse or Stone Curse, only the negative status will be removed. Blessing can be cast again to obtain the buff. [e3d63fbd38]
- **WoE**:       Emperium positions have been slightly tweaked to be closer to the center of the defined area. [288b5ffec7]
- **Mobs**:      The monster "Side Winder" has been consistently renamed to "Sidewinder". [54a5414184, 5c0fee6892, C:103edb011c, C:2ad3a014f9] OriginsRO#744
- **Mobs**:      Improvements to the Sea Otters AI configuration, to match the official settings. [07c074a55b] OriginsRO#724
- **Skills**:    The skill Benedictio Sancti Sacramenti has been simplified to check for acolyte assistants in positions relative to the map (east and west) instead of the priest's facing direction. This makes it easier to repeatedly cast the skill without having to reposition the assistants. [25daad673d] OriginsRO#547
- **Commands**:  Autolooting is now possible only if the character is within a 22 cell radius from the killed monster, in order to prevent abuse. [4345c2c213] OriginsRO#603
- **NPCs**:      Adjusted the Brave Warrior goal to be more appropriate for the current server population. [c11be85958] OriginsRO#771
- **Launcher**:  (v4.2.1 for Windows; macOS parity) Internal code improvements. [L:ceaa5501e4, L:2812184ced, L:52b31d5e89]
- **Launcher**:  (v4.2.1 for Windows; macOS parity) Improved the layout of the message boxes. [L:8066da17dd]
- **Launcher**:  (v4.2.1 for Windows; macOS parity) The launcher will now warn the user if running on an unsupported OS, where functionality and compatibility aren't guaranteed. [L:0be51cbbd8, L:ab0d33e14c]
- **Launcher**:  (v4.2.1 for Windows; macOS parity) Replace saved usernames with the corresponding email, for future compatibility. [L:eebc6f775d, L:5ed6f218bc]
- **Launcher**:  (v4.3.0; unreleased) (v4.2.2 for macOS) Internal code improvements. [L:f8a6593768, L:a59c9df718, L:f02e36bd4d, L:5a266464a5, L:9bef8d86ac]
- **Launcher**:  (v4.3.0; unreleased) (v4.2.2 for macOS) Improved the application's visual style, making it easier to differentiate focused or selected items. [L:a7aa5c80de, L:8fa37c71a1]
- **Launcher**:  (v4.3.0; unreleased) (v4.2.2 for macOS) The return key can now be used to launch the game when a character is selected from the character list. [L:21a93852dc]
- **Launcher**:  (v4.3.1) (v4.2.2 for macOS) Internal code improvements. [L:8605c7d014, L:d9a949f319, L:3bd7d656ed, L:3ab3cba646, L:00a31a6619, L:3a6e7a9353, L:e6603a0f33, L:a71d4e3137, L:9df86ee406, L:2c729ad6c2, L:68597996fe, L:7c89c8b064, L:cd45246cf0, L:997e3e9b33]
- **Launcher**:  (v4.3.1 for Windows) Support for Windows Vista has been officially dropped, following the official Microsoft product lifecycle. Users are urged to upgrade to a newer version, to benefit from security updates. [L:c039f8c6ec]
- **Launcher**:  (v4.3.1) (v4.2.2 for macOS) The application's interface has been cleaned up from some clutter. [L:5264798115]
- **Launcher**:  (v4.3.1) (v4.2.2 for macOS) Further improved the application's visual style, making it easier to differentiate between hovered and selected items. [L:78ef666dd9]
- **Launcher**:  (v4.3.1) (v4.2.2 for macOS) Improved the error report file with additional information. [L:13d98cd46a]
- **Launcher**:  (v4.3.1 for macOS) (v4.2.2 for macOS) Improved compatibility with macOS High Sierra. [L:18f09d25f5]
- **Launcher**:  (v4.3.1 for macOS) (v4.2.2 for macOS) Improved compatibility with international characters. [L:6a3c4c96a2]
- **Launcher**:  (v4.3.1 for macOS) (v4.2.2 for macOS) Support for Mac OS X 10.7 Lion and OS X 10.8 Mountain Lion has been officially dropped, following the official Apple product lifecycle. Users are urged to upgrade to a newer version, to benefit from security updates. [L:fcec5fbca5, L:ef40e1bd55]
- **Launcher**:  (v4.3.1 for macOS) Support for OS X 10.9 Mavericks has been officially dropped, following the official Apple product lifecycle. Users are urged to upgrade to a newer version, to benefit from security updates. [L:7a143a7a43, L:0a1c29a043]
- **Launcher**:  (v4.3.1) (v4.2.2 for macOS) Improved Master Account handling and saving. [L:ce025670a7]
- **Launcher**:  (v4.3.1) Internal code improvements. [L:fa9347dec6, L:ee340aee5a, L:7b764c2dee, L:d4ec969cf3, L:d289c21e63, L:f66110cd22]
- **Launcher**:  (v4.3.1) Internal changes to support the migration to a new server. [L:1cfb930d21]

### Fixed

- **Items**:     Fixed an incorrect description for Sidewinder Card. [C:8322417f9c]
- **Items**:     Fixed an incorrect description for Exorciser, Moonlight Dagger. [C:f5fe353262] OriginsRO#754
- **Skills**:    Fixed description of the Assassin Cross of Sunset skill. [C:2d566b6673]
- **Commands**:  Reworded the /shopping message to be less ambiguous. [C:1daf9da42d]
- **UI**:        Reworded some client messages. [C:198f9468d0]
- **NPCs**:      Fixed some rare cases of warps and auto-activated NPCs not sensing a nearby character. [9c4bd76b53]
- **Quests**:    Fixed an issue that could cause an excessively large amount of mobs to spawn in the Rogue Job Quest maze. [591b6cb8ff] OriginsRO#820
- **Pets**:      Removed the bonus for several pets whose taming item isn't available in the game. [800f124cc4]
- **Commands**:  The `@showexp` messages will be correctly displayed even if the character is at its maximum level. [74cd95b98a]
- **Misc**:      Fixed a rare crash that could occur when a character logs out. [7c714a0401]
- **Skills**:    The name of monsters summoned through the Hocus Pocus monster summoning skill are now correctly displayed. [3f6e0922d6, 5c9ae7b955]
- **Items**:     Fixed an issue caused by guild leader renames. Guild leaders will now need to see a GM's help to get a name change, mentioning they're a guild leader. [3994e393d4, dab2edf38b]
- **Items**:     Fixed an issue that made it impossible to pick up an item to the left of an obstacle. [1b34b3304f] OriginsRO#532
- **UI**:        Improved the display of the hourly announcer, by adding a short delay between multiple lines. [77e2f9212e]
- **WoE**:       Fixed an issue with the WoE multiclient check, potentially leaving rejected characters in an erroneous state until their next logout. [74fdd223ea]
- **UI**:        Fixed an issue that could get the login announcer stuck when an unexpectedly high amount of character log in during its one-minute interval. [2a81c90478]
- **UI**:        Fixed some inconsistencies in the party option window after logging out and back in. [0522f3c0f3] OriginsRO#534
- **NPCs**:      Fixed an incorrect message in the Sunken Ship Kafra, when saving the character position. [5de8d7c62a]
- **Skills**:    Fixed the element for skills with level greater than 10, causing certain MVP skills (Mistress JT, Drake WB, etc) to be unaffected by elemental armors. [ef51052488]
- **Misc**:      Fixed various possible buffer overflows. [6e00588ebf, 61f870f921, 4cf07e2dd8, ed8fac40e2]
- **Misc**:      Internal code improvements. [b29fb7f229]
- **Skills**:    Fixed some cell-snapping annoyances with Double Strafing and other skills, if used while walking. [cdb9812e58] OriginsRO#753
- **Mobs**:      Fixed an issue that caused some mob skills to be triggered with a smaller chance than configured, if at all (such as Sea Otters' Fatigue) [a85ffc78dd] OriginsRO#724
- **Skills**:    Fixed an issue that could cause characters to be stuck until a relog when cancelling Vending under certain conditions. [2ca1445cd6]
- **Items**:     Fixed an issue that made the Gaia Sword ineffective. [6e002e4c9f] OriginsRO#687
- **Skills**:    Restored the default minimum distance between Vending merchants and NPCs, to avoid accidental covering. [68e184be2d]
- **Maps**:      Introduced a Vending-prevention mechanism in the areas where it's forbidden to open a shop. [b047090561, f2d267cf42] OriginsRO#787
- **Mobs**:      Fixed an issue that could allow players to maliciously affect the location a killed monster would respawn in. [41d59e1dcd, 4991dc5065]
- **Items**:     Fixed an issue that could allow players to use the Gank skill with a bow, under particular conditions. [1ffd9bd9a2] OriginsRO#586
- **Mobs**:      Fixed an issue that made the Power Up NPC skill mostly ineffective. [5ecfe66d28] OriginsRO#347

## Changelog for GMs

### Added

- **Items**:     Added some future headgears to the item database for testing purposes, only accessible to GMs. [df729d2115]
- **Commands**:  Implemented the commands `@eventstart` and `@eventend`. [b7c7240a55] OriginsRO#757
- **Commands**:  Added the command `@mapinfo`. [cf1b5441e9] OriginsRO#759
- **Commands**:  Implemented the command `@evtmapflag`. [a09bd70c1e] OriginsRO#758
- **Commands**:  Implemented the commands `@evtaddwarp`, `@evtdelwarp`. [f3ef36df8a, f62b2d4c05] OriginsRO#761

### Changed

- **Commands**:  Revised the GM levels, for more fine-grained control on permissions. [a881bfb332] OriginsRO#770
- **NPCs**:      Improved the save point of the NovWarper event script. [ff75556c69] OriginsRO#763

## Changelog for Scripters

### Added

- **Commands**:  Added command `hateffect()`. [132cc722fa, 1f5fc49188]
- **Commands**:  Extended `logmes()` to work without a player attached, logging the NPC information. [0b3e2f0399]
- **Commands**:  Extended `getunits()` with the ability to perform server-wide lookups. [cb85f3d587, 769e1115ae]
- **Commands**:  Implemented the commands `setpcblock()` and `checkpcblock()`. [90be2a7937]

### Changed

- **Commands**:  The `getmapinfo()` command no longer throws a warning to the console if the map is not found. [584e8de359]
- **DBs**:       The Pet DB has been converted to a more maintainable format. [5490645ca9, f3b8178c79, 33b9216117]
- **Commands**:  The `delwall()` command will throw an error if used with an invalid wall ID. [c45d7f983b]
- **DBs**:       The Mob Skill DB has been converted to a more maintainable format. [be90f6c69b, 036ab8e2a0, 51da42665a, 99dada9177, 9d159e4215]
- **DBs**:       The Exp DB has been converted to a more maintainable format. [652d34f657]

### Fixed

* Commands:  Fixed a console error when using the `getunitdata()` script command. [d6785d389c]
* DBs:       Fixed an issue that caused the Range field of the Mob DB to be overwritten when overriding a mob with inheritance enabled. [f681c7f6fc]

### Removed

- **Commands**:  Removed custom and undocumented functionalities from `mapwarp()`. [be3c88975d]

## Changelog for Developers

### Added

- **Misc**:      Added support for soft-limiting the amount of character slots per game account. [015410d388]
- **Build**:     Simplified use of the makefile for compiling plugins, by honoring the MYPLUGINS environment variable. [9a3d423fdd]
- **Network**:   Added proxy/tunneling support, for internal testing. [38a76c4b97, c753e587a7, 179bd38139] OriginsRO#668

### Changed

- **HPM**:       The `script->sprintf()` method has been renamed to `script->sprintf_helper()` to avoid interference with system macros. [5d97abba7e]
- **HPM**:       Extended the HPM with new or split interfaced functions. [0c75c27cfb, ab54c46a7f, 66a7110fd0, 4cce05e9d2, 31400d054e, 1935ad18f5, d442a7c0bf, a0309b09be, bb0e228bd2]
- **Client**:    Added enums for the client message IDs. [d4213cd795, 73b971c66f, 031cdbe5b3, 225a7adfe9]
- **Network**:   Dropped compatibility with old login servers (versions 2016-12-27 and 2017-02-25). [cc81b74a22]
- **Logging**:   Improved the branchlog format. [b127dfb90c] OriginsRO#675
- **Config**:    Implemented live reload of the login server's permission and network settings. [e7462dffd5, b14cb3f1a0]
- **Client**:    Added/improved support for several different client versions. [6cdc68d45c, b55eddc4e2, a16333d31a, 2802489c18, 6234d9e5a1, 8e8e0a2da0, 4896facf53, 7a73c1cef1, 974c7f9824, d02ebf0532, 9ecc1bde4e, 4faf1f94fc, 21430f9a04, fd7123530c, 38e7aa7634, d6d6a61fa4, 4686ff6cfc, 44666b7203, 266f84fba5, e080479eec, 5586483c79, 4e6ed9351c, 99bc257241, 72d359867e, 5dfd713984]

### Fixed

- **Build**:     Fixed an error that caused the VS project files not to load correctly. [6f9fde965b]
- **Logs**:      Fixed some error conditions that would spam the server console. [2a39694373]

### Others

- **Merges**:    [38527550c1, 112c34e0e1, 237fd6d094, 90ce24c61f, 2bd8bcf869, ff2fe99a61, 53af9436ba, b2f92713a6, 6e0a402883, 766b03ca76, 189f2ecf6a, 81e1ace120, 32968fa05d, 3a067ed5b4, 86ca30e4d6, 21b56f6e44, 2e768a96dc, d1d38cc26a, 01902e6393, c75a0679b9, f06f7e8a29, 14432c034f, efeb386abc, d8c6912572, a4df55e940, a1f048fc6b, 24360ce305, 7593031513, b889108f6d, 141df5f2f9, 2760e208c5, 0903ea0b82, 614d7b6647, 4c4d0c3471, ed0eec5410, c8f839afd4, a6e401c504, 2269de8f39, c37149aeea, 46b0ecad01, 7bee81954a, 6d50cfd839, fc175850ef, 9370c15bc9, 8891419925, e659653465, b8cdb8e191, fa15bfc417, a3bc9ad7e7, 4cdc2b379e, b5ee7e4863, ab55a5bb31, 09885c9693, b858f305d6, 1bda48d6c3, c172c4f394, 77b6c1ecac, ccfc5b2c15]
- **API**:       Database and API rebuilds: [1b596933b1, 40bcdcba2a, a16fcb957e, b5bd99a39a, 3a0badaffa, d5ffd934ef, 1286588a17, 2bc4b4f09d, 940fce6309, 611c7db42a, 0903a1f802, ef7982e2a9, 0ba08c3ff9, 0622261073, 5ca9938288, aab3a10649, 456e3effa7, f0130ab335, c81fee18c0, 1670e2b6e5, be72d786d0, 1b48170dac, 689e8fcdfc, c35ed4341c, 376b319a28]
- **Extras**:    Extra commits merged from Hercules (not affecting or relevant to OriginsRO at this time): [6518dd8702, f4fcc01de2, d269ece7b9, 1248aa338a, 52bf3551c7, 558f279801, 4021436d86, dd6655709e, 7b097da8b4, 2a05dbfc2f, d537339c5c, 296eb152a9, 5dd8a9165d, d60a990f28, fa5cc0300a, 352bff6a1d, 394e7f8752, 5c9056c183, 34f5a72a2e, 5e5321e258, ec527a6a2d, 81d02447f7]
- **Contributions**: OriginsRO patches contributed back to the Hercules open source project: [3f6e0922d6, 5c9ae7b955, 4c4d0c3471, ef51052488, fa15bfc417]

# Previous Updates

## Changelog for Players

### Changed

- **CP**:        Minor updates to the minimaps and item images. [F:18ae59591a]
- **CP**:        Visual tweaks to the icons theme. [F:b84c7151f5, F:9018335998]
- **CP**:        The last login timestamp is now updated when logging in to the CP. [F:c8aa4cc722]
- **Website**:   Internal improvements [W:254a04d987, W:4844d5b664, W:e275389484, W:a0abd3369e, W:7644f371fb, W:81a282e8c6, W:ec147bbc1f, W:a5ec09786e, W:eed9b914a4, W:ff9b7f1708, W:e6cd972d8e, W:5b90b9cf18, W:54c984b4c5, W:d792500417, W:3e737cf12e, W:2f1005347f, W:b98ed6df08, W:55a9518e7a]
- **Website**:   Added Discord widget, cleaned up social links. [W:3ef404d3d6, W:9635b3d535, W:68c7e4abdf, W:981eb2be40, W:c5241d57b0]
- **Website**:   Improved Chat page. [W:203aacf726, W:6cb4f57967]
- **Website**:   Replaced Discord invites with permalinks. [W:4aaecb596d, W:9e651f4e4a]
- **Website**:   Updated timezone to UTC+1. [W:969eee8dfe, W:11bbe32e27]
- **Website**:   Added information about the Sunday WoE. [W:11bbe32e27]
- **Website**:   Changed links to HTTPS. [W:cac6b9881c]
- **Website**:   Updated the player count box with information about the actual meaning of the figures. Hover over the (i) with your mouse for details. [W:bb6a87cd83]

## Changelog for GMs

### Added

+ CP:        The WoE whitelist page now contains info and notes about each entry. [F:a528a31c7d]
+ CP:        Added a master account search/index function. [F:1d37dd964c]

---

Note: `F:`, `S:`, `L:`, `C:`, `W:`, `K:` have nothing to do with the emulator, but are part of, respectively, control panel, launcher-server, launcher, client, website, wiki.
