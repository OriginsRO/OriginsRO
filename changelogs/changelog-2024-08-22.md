## 2024-08-22 (Maintenance)

### Changelog for Players

#### General
- **Misc**: Added additional counter-measures for further DDoS-handling.
- **Misc**: Added rate limiting which has the side-effect that one can only login **two characters per minute**. If you want to login multiple clients, you may have to wait a bit inbetween. *This is temporary.*

#### Known Issues
- **Misc**: The game client does not support being in a full path where any non-ascii (cyrillic, japanese, chinese etc any non-latin encoding) characters are contained.
- **Misc**: The MirAI configurator shipped with the client does not support paths where any non-ascii characters are contained.
- **Misc**: Some Windows users have to change the DEP Settings for the client when they experience sporadic crashes when launching. See: https://bbs.arcadia-online.org/viewtopic.php?p=6475#p6475 for instructions.
- **Launcher**: If your Anti-Virus is screwing with the Launcher, it may fail to patch and then close itself before completion. Add an exception.
- **Launcher**: The progress reporting on patching gets stuck at times. *this should only be a visual problem*
- **Launcher**: Running the Launcher in a folder where it requires admin privileges will make it very sad and refuse to work, while being tsuntsun about it not telling you exactly why. (Yes, even when you give it admin privileges, don't do that or it might kill the entire world's kittens). A good folder to use is `C:/Users/myuser/arcadia-online`.
- **Launcher**: Some players have a different versioned `libcrypt-3-x64.dll` or `libssl-3-x64.dll` in their `windows/system32` folder overloading the Launcher's one and causing it to crash. Deleting it (Probably want to make a backup) fixes this for now. *An automagic solution is planned*.
- **Misc**: When game objects are very close to the camera they can cause visual glitches such as far stretched textures. This already existed on DirectX7 with Nvidia.
- **UI**: The partially assembled buttons (`btn_over_<mid|left|right>.bmp`, `btn_press_<mid|left|right>.bmp`) used by our custom windows have their `pressed` and `hover` state swapped.
- **UI**: The quest window uses `grp_stun.bmp` and `grp_leader.bmp` for their checkbox which is used outside of a checkbox context.
- **UI**: The guild creation window's `help` button opens a window displaying weird question-mark characters and thus has no real purpose.
- **Commands**: Using `/window` or `wi` requires relog to fully take effect. *this has always been this way apparently*
- **Misc**: The client doesn't respect where your cursor is and will always open on the primary display specified by Windows.
- **CP**: The Control Panel doesn't currently support a dark theme. Use with caution and in a well lit room.
- **Launcher**: The patcher fails to replace wrongly cased file and folder names and deletes them instead on the first run, but will re-download on the second time it runs.
- **Launcher**: In some cases, when an update deletes files that are no longer necessary, empty folders may be left behind. They can be safely removed manually, and will be removed by a future version of the launcher.
- **Misc**: Quest Log Overlay will make it impossible to click windows that were on the right-side of the screen. *this has always been that way even before DX7.*
- **Maps**: The steel plates in the water area at south east in `Kiel Dungeon F2` sometimes are invisible.
- **Launcher**: Staying logged in will eventually cause a Connection Error and require the Launcher to be restarted.
- **Misc**: The ping notification sound sometimes can't be heard when too far away.

#### Added
- **CP**: Added a check in the registration form which ensures that you read the most important part of our Terms of Service and won't create multiple master-accounts by accident. [F:f4379a2f61]
- **UI**: Added Arcadeas's Navi coordinates to Encyclopedia (Part of Relaunch Event). *You can click her sprite now to be guided to her location.* [C:3c47149ec7]

#### Fixed
- **CP**: Fixed newly created users not being able to login to the forum. [F:c6ee77c12f]
- **CP**: Fixed some typos. [701dc690ce]
- **Client**: Fixed borderless fullscreen mode not working properly for non-standard task bars. *Which resulted in gaps* [C:2f260e40c1]

> This update consists of 17 commits.
