## Update Changelog - 2021-10-18 (Ep. 10.4 halloween patch)

### Changelog for Players

#### Added
- **Events**: Enabled spooky things in Prontera. [5329a29739]
- **Events**: Re-activated Halloween 2019 and 2020 events. [cdc4ef9051, a38dbae748, e33a5a5dab, 70405fb458]
- **Events**: Added Gangster Scarf Costume to the Halloween Box of 2019. [80505350be, 5b26787c7a]
- **Mobs**: Implemented a system to remove player-summoned monsters if idle for too long. [8560914fc2]
- **Misc**: Implemented a time-out for receiving monster and NPC / tomb info for dead characters (including the `Trick Dead` skill). Related to originsro#2843 [b436e821e2, ded2ffd831]

#### Changed
- **NPCs**: Made it impossible for winners to leave the Monster Race on accident before redeeming their prize. [f443520345]
- **Skills**: Made songs not overwrite a better one outside of PvP / GvG environments. Related to originsro#2945, originsro#2894 [913df8d487]
- **Mobs**: Increased the reset timer, for non-boss monsters that are left damaged, to 60 seconds. Related to originsro#3044 [3a612db3d6]

#### Fixed
- **Quests**: Fixed a bug in the Juperos Ruins Quest trying to remove the green plate when the player starts with the blue one. [2aaed94764, d075f7ad54]
- **Skills**: Made `Ankle Snares` not dispellable to match official behavior. Related to originsro#2988 [52d8a4cf55]
- **Misc**: Included def-percent on client-side vit def only like on officials. Related to originsro#3026 [c8f958f7ce]
- **Skills**: Made food not dispellable by `Gospel` to match official behavior. Related to originsro#2933 [741d545d17]
- **Skills**: Fixed an exploit with `Leap` that gave access to areas such as the airport without paying. Related to originsro#3007 [c3aa6b1f87]
- **Other**: Made Homunculus always recalculate their speed even when resurrecting, to prevent a zoomer exploit. Related to originsro#3011 [febf88c2c1]
- **NPCs**: Fixed color formatting in the Bank quest. [6d441d8670]
- **Mobs**, **Skills**: Fixed Valkyrie not healing for 9999 on idle, to match officials. Related to originsro#3020 [d660838026]

> This update consists of 29 commits.
