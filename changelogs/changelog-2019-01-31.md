# Update Changelog - 2019-01-31 (part of Ep. 9.3)

#### Added

- **Misc**:    Added 15 new hair colors, to closely match the official colors for each hairstyle.
- **Event**:   Added the rewards for the Gunslinger vs Ninja event, and the NPCs to claim them:
  - Captain's Hat Costume
  - Gangaster Scarf Costume
  - Wanderer's Sakkat Costume
- **Items**:   Added a new costume, Smokie Hat Costume.
- **CP**:      Added a Map Database to the Control Panel, currently in beta while new features get added.

#### Changed

- **Maps**:    It is now possible to save Memo Points for Warp Portal in the Archer Village.
- **Misc**:    The '#' symbol is no longer allowed in character names, as it is not fully supported by the client.
- **UI**:      The server storage sorting criteria now include cards and refine level. Toggle between client and server sorting with the button at the bottom of the storage list.
- **Misc**:    Updated the Tan skin tone to have better contrast.
- **Misc**:    Updated various cloth colors to better match their official versions.
- **CP**:      Updated CP icons.

#### Fixed

- **NPCs**:    Fixed some minor issues in the Tailor NPC.
- **Items**:   Fixed an incorrect DEF value granted by Santa's Beard. The value now reflects the item description.
- **Party**:   Fixed some discrepancies between Control Panel and Game when listing party members.
- **Misc**:    Fixed an issue that allowed the creation of character names with forbidden symbols.

This update contains 54 commits.
