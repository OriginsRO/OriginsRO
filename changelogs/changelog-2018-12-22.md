# Update Changelog - 2018-12-22 (part of Ep. 9.3)

#### Added

- **Events**:   Christmas Event. Visit Santa's House in Lutie to get started. The Event Usher NPCs in the major towns will offer to warp you there.
- **Events**:   Christmas Bonus Event. Talk with Tobias Teppo in Lutie for additional information.
- **Events**:   The Gunslinger vs Ninja event is still running.
- **Items**:    Added 10 new costumes: Banana Hat Costume, Bone Helm Costume, Flower Hairpin Costume, Crown of Mistress Costume, Red Bandana Costume, Antlers Costume, Beanie Costume, Red Mage Hat Costume, Bongun Hat Costume, Santa Hat Costume.
- **Maps**:     Aded a no-vending area in Izlude, in the area in front of the Kafra.
- **CP**:       Added World Map to the Control Panel.

#### Changed

- **Maps**:     Changed the Guild Dungeon spawns to match the official ones, as described by iROwiki. OriginsRO#657, OriginsRO#756
- **Commands**: Extended the `@killcount` command, allowing to start the counter from any value, see `@help killcount` for details. OriginsRO#588

#### Fixed

- **Commands**: Removed the KS Protection Disabled message for those that don't have `@noks` enabled.
- **Maps**:     Improved GvG mechanics in GvG Certamen.
- **Skills**:   Changed damage from traps to be reflected by High Orc Card and Reflect Shield, to match official servers.
- **Skills**:   Fixed calculation of the damage of Blast Mine and Claymore Trap.
- **Quests**:   Fixed an issue that could get the Sage Job Quest stuck for everyone.
- **Skills**:   Fixed an issue that could cause incorrect damage calculation when a very large amount of AoE skills are stacked in the same cell at the same time.
- **Quests**:   Fixed a missing Close button in the Blacksmith Guildsman's dialogues.
- **NPCs**:     Fixed some typos in NPC dialogues. OriginsRO#1132, OriginsRO#1104
- **NPCs**:     Improved the behavior of the Comodo Gambling NPC, to avoid losing a 3-Carat Diamon when cancelling. OriginsRO#1117
- **Skills**:   Fixed issues and glitches in the Dissonance effect, when two songs or dances overlap.
- **NPCs**:     Fixed an issue in the Tailor, Hairdresser, Hair Dyer NPCs that could keep a previewed style on a character that gets recalled away. OrignsRO#1067
- **Items**:    Fixed description of the Archer Set in the Merman Card.
- **Items**:    Fixed description of the Anopheles Card for the current episode.
- **Items**:    Fixed description of Gungnir not mentioning the elemental property. OriginsRO#1193
- **CP**:       Fixed handling of special characters in passwords.
- **CP**:       Fixed table sorting functions in the game account list.

This update contains 138 commits.
