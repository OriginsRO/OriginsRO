## 2024-08-28 (Maintenance)

### Changelog for Players

#### General
- **Misc**: Added additional counter-measures for further DDoS-handling. *further ones are planned*
- **Misc**: Removed rate limit which also limited the amounts of characters one could login per minute.

#### Known Issues
- **Client**: The game prefers to be run in `C:/Users/myuser/arcadia-online`, any other folder may not have the needed permissions.
- **Misc**: Running the game in a folder that is controlled by OneDrive will result in crashes.
- **Misc**: The game client does not support being in a full path where any non-ascii (cyrillic, japanese, chinese etc any non-latin encoding) characters are contained.
- **Misc**: The MirAI configurator shipped with the client does not support paths where any non-ascii characters are contained.
- **Misc**: Some Windows users have to change the DEP Settings for the client when they experience sporadic crashes when launching. See: https://bbs.arcadia-online.org/viewtopic.php?p=6475#p6475 for instructions.
- **Launcher**: If your Anti-Virus is screwing with the Launcher, it may fail to patch and then close itself before completion. Add an exception.
- **Launcher**: The progress reporting on patching gets stuck at times. *this should only be a visual problem*
- **Launcher**, **Client**: Running any of the game binaries as administrator is highly discouraged, in the future our binaries will be tsuntsun about it and refuse to run as it might kill the entire world's kittens.
- **Launcher**: Some players have a different versioned `libcrypt-3-x64.dll` or `libssl-3-x64.dll` in their `windows/system32` folder overloading the Launcher's one and causing it to crash. Deleting it (Probably want to make a backup) fixes this for now. *An automagic solution is planned*.
- **Misc**: When game objects are very close to the camera they can cause visual glitches such as far stretched textures. This already existed on DirectX7 with Nvidia.
- **UI**: The partially assembled buttons (`btn_over_<mid|left|right>.bmp`, `btn_press_<mid|left|right>.bmp`) used by our custom windows have their `pressed` and `hover` state swapped.
- **UI**: The quest window uses `grp_stun.bmp` and `grp_leader.bmp` for their checkbox which is used outside of a checkbox context.
- **UI**: The guild creation window's `help` button opens a window displaying weird question-mark characters and thus has no real purpose.
- **Commands**: Using `/window` or `wi` requires relog to fully take effect. *this has always been this way apparently*
- **Misc**: The client doesn't respect where your cursor is and will always open on the primary display specified by Windows.
- **CP**: The Control Panel doesn't currently support a dark theme. Use with caution and in a well lit room.
- **Launcher**: The patcher fails to replace wrongly cased file and folder names and deletes them instead on the first run, but will re-download on the second time it runs.
- **Launcher**: In some cases, when an update deletes files that are no longer necessary, empty folders may be left behind. They can be safely removed manually, and will be removed by a future version of the launcher.
- **Misc**: Quest Log Overlay will make it impossible to click windows that were on the right-side of the screen. *this has always been that way even before DX7.*
- **Maps**: The steel plates in the water area at south east in `Kiel Dungeon F2` sometimes are invisible.
- **Launcher**: Staying logged in will eventually cause a Connection Error and require the Launcher to be restarted.
- **Misc**: The ping notification sound sometimes can't be heard when too far away.

#### Added
- **UI**: Added possibility to set a key binding for `Game Setting Window` (the one that lets you go to Char Selection). [C:858d04de05, C:04d3cdafeb, C:688d229578]
- **UI**: Added configurable `Bank` Hotkey, which is binded to `ALT + B` by default. [C:c40296bbaa]

#### Changed
- **Launcher**: Changed version string for Windows 11 to its full version (Windows 11 21H2), to avoid confusion in the deprecation messages. [L:2e787d8255]

#### Fixed
- **Skills**: Fixed 13 skills not being plagiarizable, namely: Related to arcadia-online#3598 [1bd51c4d58]
  - `Arrow Shower`
  - `Pierce`
  - `Brandish Spear`
  - `Spear Stab`
  - `Spear Boomerang`
  - `Fire Pillar`
  - `Land Mine`
  - `Blast Mine`
  - `Claymore Trap`
  - `Charge Arrow`
  - `Holy Light`
  - `Grand Cross`
  - `Asura Strike`
- **NPCs**: Fixed a mysterious `kick_trigger` appearing in Sideworld. [29c9bedab7]
- **UI**, **Misc**: Fixed some saving issues of the equipment sets feature. [a07eb33a37]
- **CP**: Fixed an exception when using the item finder to search for an item that has no icon. [F:25d4be6617]
- **CP**: Fixed email change not working. Related to arcadia-online#3611 [F:4c1a26103e]
- **Client**: Fixed not resetting effect states when needed, causing the entire game to have its colors negated. [C:169f433825]
- **Client**, **Items**: Fixed `Alternative Summer Costume` causing a crash when used with `Santa's Bag` on login. [C:f6c7b4fad2]
- **Client**, **Commands**: Fixed remembered `/mineffect` state not being applied on login. [C:b219fc0295]
- **Client**, **UI**: Fixed `combined card's window` opening outside of screen on higher resolutions. [C:278fdbc9d4]
- **Client**: Made books less crashy on some systems, but instead display empty content on failure. [C:38cf07e92e]

> This update consists of 46 commits.
