## Update Changelog - 2021-07-04 (Ep. 10.4 hotfix patch)

### Changelog for Players

#### Added
- **NPCs**: Added the possibility to randomize all remainings numbers in Hugel Bingo. Related to originsro/originsro#2914 [73602f3ae3]
- **Maps**: Added missing red dots for warp portals on the minimaps of hu_filld01, hu_fild02, hu_fild04, hu_fild05 and ein_fild06.

#### Changed
- **NPCs**: Changed the "Bingo!" in the Hugel Bingo dialogue to "Bingo" as that's expected to be typed in. [6bc32b7d1b]
- **Items**: Synchronized the following items with their clientside name: [4c29798320, e72c6bf492, d44af4e45b, 8d10014fd2]
  - Resist Potions Related to originsro/originsro#2293
  - Memory Bookmark Related to originsro/originsro#2787
  - Lollipop Related to originsro/originsro#1950
  - Nutshell & Nutshell Costume originsro/originsro#2880
- **Items**: Synchronized the following items with their serverside name: [C:562bb1ea08, C:13b0c7a408]
  - Prize Medal
  - Ayam & Ayam Costume Related to originsro/originsro#2880
- **Items**: Fixed Prize Medal description showing 1 weight, when it's 0 in fact. [C:562bb1ea08]

#### Fixed
- **Items**: Fixed stat food not working in War of Emperium. Related to originsro/originsro#2915 [73ae99f7e4]
- **NPCs**: Fixed Dual Monster Races never finishing. Related to originsro/originsro#2913 [53febed93a, 5373f8b72c, 572238ab84]
- **Skills**: Fixed the following skills not dealing any damage: [cb24467888, 0d802b6b89]
  - Venom Splasher
  - Meteor Assault Related to originsro/originsro#2917
  - Pulse Strike
  - Hell Judgement
  - Vampire Gift
  - Fire Breath
  - Ice Breath
  - Thunder Breath
  - Acid Breath
  - Darkness Breath
- **Skills**: Fixed a wrong statement in the skill description of `Sprint`. Related to originsro/originsro#2912 [C:c385661140]
- **Mobs**: Fixed Phreeoni on moc_fild15 not having a static Tomb spawn. Related to originsro/originsro#2916 [ff65d01731]

> This update consists of 18 commits.
