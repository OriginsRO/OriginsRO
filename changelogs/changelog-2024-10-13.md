## 2024-10-13 (Maintenance)

### Changelog for Players

#### General
- **Event**: Relaunch Event still running, check it out: https://wiki.arcadia-online.org/Relaunch_2024/
- **DB**: Some database clean-up, if your lost homunculus suddenly appeared and you had an issue open, please close it.

#### Known Issues
- **Client**: The game prefers to be run in `C:/Users/myuser/arcadia-online`, any other folder may not have the needed permissions.
- **Misc**: Running the game in a folder that is controlled by OneDrive will result in crashes.
- **Misc**: The game client does not support being in a full path where any non-ascii (cyrillic, japanese, chinese etc any non-latin encoding) characters are contained.
- **Misc**: The MirAI configurator shipped with the client does not support paths where any non-ascii characters are contained.
- **Misc**: Some Windows users have to change the DEP Settings for the client when they experience sporadic crashes when launching. See: https://bbs.arcadia-online.org/viewtopic.php?p=6475#p6475 for instructions.
- **Launcher**: If your Anti-Virus is screwing with the Launcher, it may fail to patch and then close itself before completion. Add an exception.
- **Launcher**: The progress reporting on patching gets stuck at times. *this should only be a visual problem*
- **Launcher**, **Client**: Running any of the game binaries as administrator is highly discouraged, in the future our binaries will be tsuntsun about it and refuse to run as it might kill the entire world's kittens.
- **Launcher**: Some players have a different versioned `libcrypt-3-x64.dll` or `libssl-3-x64.dll` in their `windows/system32` folder overloading the Launcher's one and causing it to crash. Deleting it inside the launcher's folder fixes this for now. *An automagic solution is planned*.
- **Misc**: When game objects are very close to the camera they can cause visual glitches such as far stretched textures. This already existed on DirectX7 with Nvidia.
- **Commands**: Using `/window` or `wi` requires relog to fully take effect. *this has always been this way apparently*
- **Misc**: The client doesn't respect where your cursor is and will always open on the primary display specified by Windows.
- **CP**: The Control Panel doesn't currently support a dark theme. Use with caution and in a well lit room.
- **Launcher**: The patcher fails to replace wrongly cased file and folder names and deletes them instead on the first run, but will re-download on the second time it runs.
- **Launcher**: In some cases, when an update deletes files that are no longer necessary, empty folders may be left behind. They can be safely removed manually, and will be removed by a future version of the launcher.
- **Misc**: Quest Log Overlay will make it impossible to click windows that were on the right-side of the screen. *this has always been that way even before DX7.*
- **Maps**: The steel plates in the water area at south east in `Kiel Dungeon F2` sometimes are invisible.
- **Launcher**: Staying logged in will eventually cause a Connection Error and require the Launcher to be restarted.
- **Misc**: The ping notification sound sometimes can't be heard when too far away.
- **Client**: Under rare circumstances the client can *steal your mouse* resulting in you not seeing your cursor, using `windows key` + `L` and logging back in fixes it.

#### Added
- **NPCs**: Added three new fluff NPCs to Sideworld. [bd0f07f21e, 48e83d0470]
- **Quests**: Added safe guard against brewed item dropping due to full inventory (weight & count) from atelier. [f9bbc5b198, 4d03b268c9]
- **NPCs**: Added partial refund to Name Change Ticket from Civil Registry NPC. [d7de3148e5]
- **NPCs**: Added safe guard against Name Change Ticket dropping due to full inventory from Civil Registry. [05b36fafba, 8584bf4848, cd2302f73c]
- **NPCs**: Added instructions on using Name Change Ticket from Civil Registry in several moments of the NPC's dialogue. [a22250ae36, 3ebaf064be]
- **NPCs**: Added red exclamation marked warning about re-rebirthing for reading-disabled people. [d8b7313cdc]
- **Commands**: Enabled usage of `@noks` in town. [6be63a75d5]
- **NPCs**: Added stat hexagon warning at several dialogues in stat refunder. [72b8b9e57e, 2fd634bbbd]
- **WoE**: Implemented simple and random castle rotation for pretrans WoE. Related to arcadia-online#3143 [97219821fe, 22f8c368ba]
- **WoE**: Added castle owner flags for pretrans WoE session in the cities having castles. [dc7ca9cbc5, 8cf755a0ca]
- **Misc**: Implemented custom error message for when login session runs out when waiting outside character selection. [100bdc9662, C:1d7cf6d36f, C:860b5c4478]
- **CP**: Added captcha to the login page. [F:d1e6cca23b]
- **CP**, **Guilds**: Made guild storage viewable for all members that have access. *does not include the logs* Related to arcadia-online#2950 [F:f8aad2be88]
- **Items**: Added instructions to Name Change Ticket description. [C:b296f4942a]
- **UI**: Added 2 new entries to the Encyclopedia. *Information regarding experience and the summoning flutes* [C:b0a4dac341]
- **Items**: Added missing job in `Pocket Watch`'s description. Related to arcadia-online#3824 [C:821d840b41]
- **Items**: Added missing information in `Grand Cross` item description. *It grants Level 3 Turn Undead* Related to arcadia-online#3786 [C:ab92030eb9]
- **UI**: Added new loading screens. [B:d4c1e02864, B:b3c2c0bf2e]
- **Launcher**: Added display of EOL date of the current operating system when unsupported. [L:e763c90224]
- **Launcher**: Added the party/friends list lock toggle to the settings. *Previously, the setting was wrongly always forced to locked* [L:a4c34dacf5]
- **Items**: Added additional effects to fishing consumables. [e885448ab9]

#### Changed
- **Pets**, **Homunculi**: Made it so that Homunculi & Pets warp back to master if out of range on hunger timer as well. [4a0c94ff0e, 76fa0aa892]
- **Mobs**: Reduced Poring Coins mob spawn count to one per city in Sideworld. [f57b632e66]
- **Commands**, **Misc**: Made it so that players that have `@noks` off are protected as long as someone with `@noks` on tries to take their mob. *If you want to be ks-protected as a group you should all use `@noks party` or `@noks guild`, otherwise you will fail to attack.* [8266157264]
- **Commands**: Applied killsteal protection already when being attacked by a monster. [c15e525994]
- **Items**: Matched `Blue Potion` Buy/Sell Price with `White Potion`'s. *To discourage people from selling it to the NPC.* [140deb4668]
- **NPCs**: Reformatted text to be less red and more in style with the rest of the npc script in stat refunder. [5b435bceaf]
- **NPCs**: Reworded two menus options to more clearly communicate the intent in stat refunder. [928e0f5d15, 4b0586c1e1]
- **Skills**: Reworded the `Warp Portal` failure when trying to open one from a non Sideworld map to a Sideworld map. [f230943ce6]
- **CP**: Cleaned up unnecessary URL parameters in the login page. [F:112d26cc06]
- **CP**: Update some gitlab URLs to the arcadia-online org one. [F:052359b10d]
- **Skills**: Updated description of `Spiritual Potion Creation I` with `Blue Potion` mention. [C:0ef89befee]
- **Items**: Improved Succubus's taming description. [C:eeb5553e05]
- **Items**: Changed Faux Image to Fausse Image in client-side. [C:efeac1da80]
- **UI**: Updated bio3 info to 2020 changelog in Encyclopedia. [C:13dc0b269a]
- **UI**: Used proper name for EXP bonus in Encyclopedia. [C:fdb251fb6f]
- **Skills**: Synchronized `Magnificat` description with server-side. Related to arcadia-online#3736 [C:e0975ebd83]
- **UI**: Changed checkbox in Encyclopedia to a normal one instead of colliding party symbol. [C:c328cae99f]
- **Launcher**: Made the UNSUPPORTED SETUP message more visible. [L:708a449a7c, L:ab9c455354]
- **Launcher**: Reworded the Connection error message to give more self-troubleshooting tips. [L:7a84eeb4d5]

#### Fixed
- **Misc**: Fixed a bug where one becomes unable to login when renaming a character in a full party. [a9ca7fe20e, b32ad7ccb6, C:47070a71c1]
- **Quests**: Fixed some typos in Atelier Richelle. [ad54b6408c, 492a681cd2, fd553a6ec2]
- **Pets**: Made Pets warp to their owner when out of range without chance to catch up. *such as when backsliding across the map* Related to arcadia-online#3655 [b1df8fe038]
- **Quests**: Fix being able to talk to guild master in maze part of Assassin Job Change Quest before reaching him. Related to arcadia-online#3610 [162ada1130]
- **Quests**: Corrected atelier air element text colour from green to dark yellow. [61821d90f8]
- **QuestS**: Fixed Sideworld entrance quest instance part timing out before finishing loading into the map. [913a810479]
- **NPCs**: Fixed prayers in Sideworld. [7d4dcea717]
- **Quests**: Fixed a stuck dialogue when finishing the instance part of the Sideworld Entrance Quest under certain conditions. Related to arcadia-online#3681 [c11ffa81f0]
- **Quests**: Fixed typos in Sideworld Entrance Quest. [6fc274449d, 957eca452f, 63e6dfe3bc]
- **NPCs**: Fixed Tailor NPC softlock upon pressing cancel button while browsing wardrobe. Related to arcadia-online#3726 [0af2e6d763]
- **NPCs**: Fixed Tailor NPC stacked dialogue after buying cloth to wardrobe. [c56cd5cdbe]
- **Skills**: Ensure that `Teleport` cannot be used when no longer owning the skill after selecting the target. [6ee1dc9f3c, 3250fbb90e]
- **Skills**, **Items**: Fixed temporarily granted skills remaining useable even after for example unequipping the item granting them. [75fb398eee]
- **NPCs**: Fixed stat refunder showing incorrect values for stat limits of Babies. [f329ff737b]
- **Skills**: Fixed previously plagiarized skill re-appearing after login, when removed due to lower level copy failure. [89b4796247]
- **NPCs**: Fixed some important dialogues not appearing in stat refunder. [9ae621cb2d]
- **Skills**: Fixed `Snap` failing to be used when still in its allowed range. Related to arcadia-online#3798 [7f29c21f1d]
- **Homunculi**: Fixed a scenario in which one creates several homunculi by accident and ends up with a Lv. 1 one when relogging. [15f58c21b8, c6237b2113, 53c2b1eb1c, 200a92e613, f0fb900005]
- **CP**: Fixed a wrong url in the index page and avoid showing unusable links. [F:cea73fef95]
- **CP**, **Homunculi**: Fixed an error in the homunculus information page for Amistr. [F:234eeceb77]
- **Items**: Fixed the description of White Lady's taming item. [C:14fc92dbb8]
- **Items**: Fixed incorrect / mismatching item names. *Grove Card -> Fruit Mix, Bent Spoon -> Spoon Stub* [C:19ab0329b8]
- **UI**: Prevented some unusual crashes when opening Encyclopedia Window on some weird systems. [C:de6f8c4f57]
- **Client**, **UI**: Fixed `combined card's window` opening outside of screen on higher resolutions. *now for real, as the fix wasn't active* [C:3c006ec21a]
- **UI**, **Guilds**: Fixed guild emblem not showing before opening guild window in some cases. [C:6a045ab398]
- **UI**: Fixed custom buttons *(used by Equip Sets, Restock Window etc)* having their hover and press state swapped. [C:2644bff6d9]
- **UI**: Fixed items in Restock Window being initialized with incorrect information. Related to arcadia-online#3710 [C:57f5ebca28]
- **Skills**: Fixed `Spiritual Potion Creation I` Lv. 2 crashing with `/effects` off. Related to arcadia-online#3794 [C:0796376577]
- **Mobs**: Fixed monsters being unattackable / untargetable until they move under some conditions. *also potentially some visual movement bugs of mobs* [C:01cc5ac6be, C:6c25336e48, C:a6f121e209, C:b3682b0450]
- **UI**: Fixed guild creation window's help button opening a window displaying weird question-mark characters. *Now with useful information* [B:57f913a282]
- **Misc**: Fixed a DEP related crash that some users had. [B:429ef5623c]
- **UI**: Fixed world map data to original image. [B:a1e8c1bbc3]

> This update consists of 165 commits.
