# Update Changelog - 2018-09-06 (part of Ep. 9.3)

### Notes

## Changelog for Players

### OriginsRO Episode 9.3 content

- **Events**: Gunslinger vs Ninja event. See the [episode introductory post](https://originsro.org/originsro-episode-9-3) for more information.
- **Items**: Added 5 new costumes:
  - Zealotus Mask
  - Incubus Horn
  - Succubus Horn
  - Crown of Ancient Queen
  - Spiky Band
- **Classes**: Ninja and Gunslinger job classes. Some skills have been nerfed in WoE maps, to better fit a pre-trans environment (the modifications may be subject to further changes if necessary for game balance purposes):
  - Cast-off Cicada Shell: the cast time of Urgent Call is increased by 1 second for each stack;
  - Desperado: damage reduced by 40% in WoE;
  - Final Strike: the caster becomes immune to Devotion for 10 seconds in WoE;
  - Full Buster: damage reduced by 25% in WoE;
  - Illusionary Shadow: the cast time of Urgent Call is increased by 1 second for each stack;
  - Kamaitachi: damage reduced by 25% in WoE;
  - Rapid Shower: damage reduced by 25% in WoE;
  - Throw Coins: damage reduced by 40% in WoE.
- **NPCs**:   Shops for Ninja and Gunslinger gears and consumables.
- **Quests**: Quests for Gunslinger and Ninja weapons, armors and bullets.
- **Quests**: Zealotus Mask quest.
- **Quests**: Platinum Skill quests:
  - Throw Venom Knife
  - Sonic Acceleration
  - Dubious Salesmanship
  - Greed
  - Phantasmic Arrow
  - Charge Attack
  - Redemptio
  - Close Confine
  - Sight Blaster
  - Elemental Potion Creation Guide
  - Pang Voice
  - Shrink
  - Charming Wink
  - Spiritual Bestowment
  - Excruciating Palm
  - Create Elemental Converter
  - Elemental Change
- **Skills**: Taekwon Mission and the ranked Taekwon game mechanics (the HP/SP bonus has been reduced from 300% to 150%, to better fit a pre-trans environment - this change may be subject to further changes if necessary for game balance purposes).
- **Maps**:   Einbroch Fields 1 through 5, Geffen dungeon F4, and their appropriate monsters.
- **Items**:  Items related to Ninja, Gunslinger and the newly introduced monsters.
- **Items**:  The following, previously disabled, items, have been re-enabled and made usable: Violin[4], Huuma Calm Mind, Gate Keeper-DD, Long Barrel, Lever Action Rifle, Violy Card, Succubus Card, Incubus Card, False Angel Card.
- **Items**:  Dagger of Counter has been re-added to the RSX-0806 drops. OriginsRO#682

### Added

- **Launcher**: (v4.3.2) Added an option for simplified effects (AoE opacity and reduced song duration) - OriginsRO#821, OriginsRO#822 - simplified effects courtesy of Kergal
- **NPCs**:     Added an NPC to sell Name-Change tickets, available at the Kafra Headquarters. - Note: this was already applied as hotfix before the maintenance
- **Commands**: Added `@gstorageaccessconf` as an easier to remember (and to search) alias for the `@gsacl` command.
- **CP**:       Added total online time stats to the character view pages. OriginsRO#847
- **Network**:  Added experimental support for a failover server connection in case the main server is unreachable, and a proxy server in the APAC area, to reduce connection delay. The following options are available at the service selection screen:
  - OriginsRO: the main server connection (same as previously available), recommended for users from Europe and America
  - OriginsRO (Failover): secondary connection, to troubleshoot connectivity problems or in case of outages affecting the main connection
  - OriginsRO (APAC Proxy): proxy server optimized to reduce connection latency for users from the APAC area (especially south-eastern Asia and Oceania). Please note that the effectiveness of this proxy server for lag-reduction purposes may depend on the user's ISP and network conditions.

### Changed

- **NPCs**:   The Guild Storage pricing has been made consistent (always 10x the normal storage price).
- **NPCs**:   The Kafra services prices are now always displayed before charging the player. OriginsRO#828
- **Maps**:   Renamed PvP Bellum to GvG Bellum.
- **Skills**: When a buying store fails to open because on an illegal cell, the license is now returned.
- **Skills**: Added warning about the existing 2% tax to the Vending skill.
- **Skills**: The vending illegal areas have been extended to chats.
- **Skills**: Fixed the spelling of "Lightning" skills.
- **Skills**: Fixed the Permanent Development guild skill name.
- **Skills**: Fixed the spelling of "Jupiter".
- **Mobs**:   Updated the spelling of the Abyssal monsters. OriginsRO#892
- **Mobs**:   Fixed the spelling of Arclouse and its plural form. OriginsRO#893
- **Items**:  Fixed the spelling of Cutlass. OriginsRO#890
- **Items**:  Adjusted drop rates of item chains (Ore Discovery, Gaia Sword, Jewel Sword, Blazer Card, Tengu Card) to better fit the server's drop rates. OriginsRO#750
- **Items**:  The Infinitus potions can no longer be traded/vended, and are now automatically removed from inventories when leaving the PvP lobby. OriginsRO#909
- **NPCs**:   Changed the PvP Cat's menu to make it easier to leave the PvP lobby. OriginsRO#789
- **NPCs**:   Clarified the armor requirement in the Moving HP Recovery Quest.
- **Misc**:   The slots available for character creation have been reduced to 6. We encourage the creation of separate game accounts (within the same master account) to manage larger amounts of characters. Characters already created in slots 7-9 can still be accessed without any restrictions.
- **Maps**:   The size of the areas protected from chat/shop creation in front of the Prontera inns has been increased, to make it easier to walk into and out of the inns. OriginsRO#933
- **Maps**:   Repositioned the PvP Lobby save point to avoid covering the PvP Cat. OriginsRO#789
- **CP**:     Updated the timezone to UTC, to match the game server.
- **UI**:     Updated loading screens.
- **UI**: (Launcher v4.3.2, Game Client, CP) Removed gender-specific job names from the game, for gender parity.
- **CP**: Removed legacy username based login. Only the e-mail is now accepted as username.

### Fixed

- **NPCs**:   Fixed an error causing some Kafras to hang and force the player to relog - Note: this was applied as hotfix, before the maintenance
- **NPCs**:   Reworded and fixed the dialogues of the Brave Warrior NPC.
- **Skills**: Dead party members are now ignored by the Kihop bonus, in order to prevent abuse.
- **Misc**:   It's now impossible to create characters with a 2-letter `[xx]` prefix, to prevent staff impersonation.
- **Skills**: Fixed an issue that could cause a character to get stuck until a relog, when opening a shop in forbidden maps.
- **Maps**:   Disabled mob drops in the Monk jobchange map, to prevent abuse.
- **Maps**:   The `prt_fild05` map is now correctly classified as "near town" / "newbie area".
- **Maps**:   Fixed an issue that allowed guildless characters to access the guild dungeons from an empty castle. - Note: this was applied as hotfix, before the maintenance
- **Mobs**:   Fixed aggressive ranged mobs not attacking from the top of a cliff. OriginsRO#865
- **Skills**: Prevented skills that fail or aren't applicable against a given monster from counting as an "exp tap". OriginsRO#870
- **Skills**: Fixed the Bard/Dancer skill effects to no longer persist after death. OriginsRO#773
- **Items**:  Weapon and arrow switching is no longer blocked while attacking. OriginsRO#586
- **Items**:  Fixed Sea Otter Card not increasing the Sushi healing rate.
- **Items**:  Removed misleading information about pet bonuses from the eggs of pets that currently have none.
- **Items**:  Removed false information about Wedding Dress having a 15 MDEF bonus.
- **Items**:  Clarified the description of Geographer Card.
- **Items**:  Reworded the description of Assassin Mask. OriginsRO#697
- **Skills**: Reworded and clarified the description of Occult Impaction.
- **Skills**: Reworded and clarified the description of Increase SP Recovery.
- **Skills**: Fixed an issue that allowed Monocell and Class Change (???) from Hocus-Pocus to be cast in maps with mob summoning restrictions.
- **Skills**: Fixed an issue that caused skill cooldowns to get stuck indefinitely. OriginsRO#871, OriginsRO#843
- **Skills**: Fixed an exploit that would allow to extend the duration of the out-of-song effect of bard/dancer skills from 20 seconds to the full song duration. OriginsRO#833
- **Skills**: Fixed the double consumption of gemstones when casting Venom Splasher. OriginsRO#875
- **UI**:     Changed the default value for the `/showname` setting on newly installed clients to use the thin font and extended information.

# Update Changelog - 2018-09-08 (supplementary update)

### Fixed

- **Items**: Fixed an issue that caused certain forged or brewed items to display as if created by `Mockingbird`.
- **Pets**:  Fixed an issue that caused pet eggs to disappear when sending a pet back to its egg.
- **Maps**:  Fixed a nonwalkable area in Einbroch Field 1 that would cause characters to get stuck when entering from the south-eastern warp.
