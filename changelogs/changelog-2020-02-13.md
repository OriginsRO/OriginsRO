# Update Changelog - 2020-02-13 (part of Ep. 10.2)

#### OriginsRO Episode 10.2 Content

- **Maps**: Added some maps in the area east of Juno.
  - El Mes Plateau (`yuno_fild06`)
  - Juno Field (`yuno_fild10`)
  - Hugel Field (`hu_fild04`)
  - The Abyss Lakes (`hu_fild05`)
  - Hugel Field (`hu_fild07`)
- **Maps**: Added the Abyss Lakes dungeon.
  - Abyss Lakes Underground Cave B1 (`abyss_01`)
  - Abyss Lakes Underground Cave B2 (`abyss_02`)
  - Abyss Lakes Underground Cave B3 (`abyss_03`)
- **Items**: Added the items related to the newly introduced maps and quests, including those that were previously disabled.
- **Mobs**: Added the mobs spawning in the newly introduced maps.
- **Classes**: Added the Star Gladiator and Soul Linker classes and related content.
- **Quests**: Added the Magistrate Hat / Ayam quest.
- **Items**: Added 12 new costumes
  - Flu Mask Costume (200,000 Z)
  - Gas Mask Costume (200,000 Z)
  - Super Novice Hat Costume (200,000 Z)
  - Masquerade Costume (380,000 Z)
  - Safety Helmet Costume (380,000 Z)
  - Straw Hat Costume (380,000 Z)
  - Blue Hairband Costume (500,000 Z)
  - Opera Masque Costume (500,000 Z)
  - Tulip Hairpin Costume (500,000 Z)
  - Ayam Costume (750,000 Z)
  - Frog Hat Costume (750,000 Z)
  - Magistrate Hat Costume (750,000 Z)

#### Added

- **Chat**: Added a chat filter for the chat channel messages, to allow diverting them to a separate window or filtering them out. originsro/originsro#1785
- **Chat**: Added the possibility to customize the chat channel colors, for each player. (Note: this feature is currently in a beta testing stage, and will be rolled out gradually to everyone)
- **Misc**: Added an option to permanently increase the Master Storage capacity, up to 1,000 slots. Visit the Kafra Headquarters in Aldebaran for more details. (Note: the maximum capacity expansion limit may be increased further in future updates).
- **UI**: Added the possibility to customize the opacity of the chat boxes and message bubbles through the `/chatalpha` command (Note: a version of this configurable in the CP for an entire Master Account is currently in a beta testing stage, and will be rolled out gradually to everyone).
- **Homunculus**: Fixed/enabled the missing Homunculus feeding animations.
- **Misc**: Added an option to restrict equipment trades when enabling Guest Access. originsro/originsro#451 originsro/originsro#1761
- **Misc**: Added an option to restrict guild invitation and expulsion commands when enabling Guest Access. originsro/originsro#1438
- **Skills**: Added status icons for Aura Blade and Concentration.

#### Changed

- **Events**: Updated the Mob of the Day exclusions to better reflect the current episode. originsro/originsro#1721
  - Alice is now excluded again
  - Golden Acidus is now excluded
  - Sleeper is now excluded
  - Stalactic Golem is now excluded
  - The monsters in `lhz_dun03` are now excluded
  - The monsters in `mag_dun01` are now excluded
  - The monsters in `mag_dun02` are now excluded
  - Bloody Murderer is no longer excluded
  - Fur Seal is no longer excluded
  - Loli Ruri is no longer excluded
  - Orc Archer is no longer excluded
  - Sea Otter is no longer excluded
- **Maps**: Renamed Morroc to the official name, Morocc.
- **Misc**: Minor reliability and performance improvements.
- **Maps**: Extended the no-vending zones of Aldebaran, Comodo and Payon. originsro/originsro#1786
- **Chat**: Extended the maximum length of the chat channel messages.
- **Skills**: The Homunculus skill cooldowns (except Urgent Escape) are now reset on (the homunculus') death. originsro/originsro#1744
- **Skills**: Disabled the Moonlit Water Mill skill in towns, to avoid abuse. originsro/originsro#1783
- **Skills**: Marionette Control is now cancelled if it's active when assigning stat points. originsro/originsro#1758
- **Chat**: Changed the default color of the `#support` channel to 'Emerald'. originsro/originsro#1757
- **Skills**: The bonuses from Gospel (that are removed when going through a Warp Portal) are now also removed when logging out. originsro/originsro#1782
- **Misc**: The Guild Storage guest restriction now also prevents use of the `@gsacl` command. originsro/originsro#1438
- **Skills**: Improved the Hunter trap damage, with customized renewal-inspired formulas, adapted for pre-renewal game mechanics. See the wiki for more details and information on the rounding behavior of the divisions. (Note: the damage formulas are subject to change at any time without prior warning, and no skill/stat resets will be provided) originsro/originsro#1237 originsro/originsro#1649
  - Claymore Trap: `skill_lvl * (dex / 2) * (3 + base_lvl / 85) * (1 + int / 35)` (each monster in the area receives the full damage)
  - Blast Mine: `skill_lvl * (dex / 2) * (3 + base_lvl / 100) * (1 + int / 40)` (each monster in the area receives the full damage)
  - Land Mine: `skill_lvl * (dex / 2) * (3 + base_lvl / 50) * (1 + int / 25)`
- **Map**: Added `pay_fild02` to the maps protected from Dead Branches, Class Change, Summon Monster.
- **Misc**: 34 new/modified clothing dyes. These palettes are replacing dyes 13-46, and include both edits to currently implemented dyes, as well as the reintroduction of the 11 dyes that were removed in an earlier patch. The original 11 dyes have been modified accordingly to better fit each individual class. If your character is currently wearing one of these palettes, you will automatically be given a free clothing coupon that you can redeem at the tailor NPC in southwest Prontera. Please keep in mind that your dye may have been altered or removed, and even if it was not, the number will have shifted and you will need to re-purchase it. Note: This will not affect any non-expanded first class characters or novices.
- **UI**: Changed the message length limit in the 1:1 Chat window to be the same as the regular chat.
- **Commands**: Improved `/mineffect` to show the healing effect on Emperium without having to use `/showheal`.
- **Misc**: Improved the Storage/Master Storage/Guild Storage loading speed when there are several items in it.

#### Fixed

- **Skills**: Fixed an error that made some skills not level-selectable (already patched in the live server).
- **Chat**: Fixed the time displayed by the chat channel cooldown system in particular cases.
- **Quests**: Fixed a question in the Blacksmith job change quest.
- **Commands**: Fixed the time displayed by certain `@`-commands.
- **UI**: Fixed the HP bars not getting updated in some cases.
- **Quests**: Fixed some minor errors in the President quest.
- **Misc**: Fixed a client crash when using the Navigation system to reach certain NPCs in Lighthalzen.
- **Skills**: Fixed Asura Strike's combo chainability.
- **Items**: Fixed the beloved pets' eggs not getting marked as such.
- **Skills**: Fixed the animation of the Back Sliding skill.
- **Skills**: Fixed the visibility of the Chase Walk footsteps in GvG/PvP.
- **Misc**: Fixed an issue that caused characters dying with the Item Appraisal or similar windows open to be unable to use skills, items and NPCs upon resurrection.
- **Misc**: Fixed the mute sign not getting correctly displayed on login.
- **Skills**: Fixed the Lord Knight's Concentration animation when entering in the view range of other players.
- **Skills**: Fixed the equipment-breaking behavior of Tarot Card to only target left hand (shield), armor and helm.
- **Items**: Fixed the racial crit bonus not being affected by the katar crit bonus.
- **Skills**: Fixed Lex Aeterna not getting removed by the stone and freezing statuses.
- **Misc**: Updated some incorrect login announcement messages.
- **Skills**: Added the hidden functionality of the Undying Love skill, when used by a female super novice, allowing her to transfer a +1 stat to her partner for 5 minutes at a low chance. originsro/originsro#644
- **Misc**: Fixed an issue that prevented trading with dead characters.
- **Pets**: Fixed an issue that could cause several visual copies of a pet to be spawned and persist in a map.
- **Commands**: Fixed the `@autotrade` command not restoring the character's orientation correctly after a server restart.
- **Misc**: Fixed the mute status blocking certain server-issued commands.
- **UI**: Fixed an issue that made it impossible to click on an item slot after a failed transfer to the cart (because of the weight limit).
- **Skills**: Fixed the Tomahawk Throwing skill so that it can be copied with Plagiarism. originsro/originsro#689
- **Items**: Fixed some costume overlap issues in WoE maps.
- **Commands**: Fixed an issue that made the `@noks` protection fail completely in some specific scenarios. originsro/originsro#1797
- **UI**: Fixed a glitch in the Skill window when hiding and unhiding or wearing a wedding dress while riding a Peco. originsro/originsro#1775
- **Maps**: Fixed the PvP Infinitus and GvG Certamen maps so that they allow use of the Infinitus Potions and keep the PvP or GvG mechanics, rather than one or the other.
- **Skills**: Fixed the homunculus skills so that they're usable when the current amount of SP is exactly the amount required by the skill. originsro/originsro#1719
- **Misc**: Fixed an exploit potentially allowing the idle timer to be silently reset with an appropriately forged packet, by using a modified client.
- **Skills**: Changed the damage reflection mechanics (Shield Reflect) not to trigger if the damage was blocked before hitting the character (i.e. Safety Wall). originsro/originsro#1773
- **Maps**: Fixed the pits in the Ancient Shrine Maze (Ayothaya Dungeon lvl 1).
- **Mobs**: Fixed an issue in the monster AI causing ensnared monsters to forget their target.
- **Skills**: Fixed Marionette Control not giving the expected bonus when an existing bonus was present and greater than zero (such as the job bonus). The value is now capped, as expected, to the smallest between `99 - [base stat]` and `99 - ([base stat] + [existing bonus])` at the moment the skill is cast.
- **NPCs**: Removed redundant dialogues from the Utan Shaman NPC. originsro/originsro#1149
- **Maps**: Removed a duplicate warp (resulting in a brighter warp portal) from Somatology Laboratory F2 (`lhz_dun02`).
- **Skills**: Fixed an issue that caused the guild aura effects to behave unexpectedly when the guild leader dies and is resurrected.
- **Skills**: Fixed a position desynchronization that could happen when sitting right after using Snap (and possibly other skills).
- **Items**: Fixed the position of the Candy Cane in Mouth. originsro/originsro#1769
- **Maps**: Relocated the Umbala Inn so that it no longer conflicts with The Sign quest.
- **Misc**: Fixed lower than expected damage when attacking with a Rifle type weapon.
- **Skills**: Fixed an issue that could cause Vanilmirths to disappear after killing their own master through the Self Destruction skill.
- **UI**: Fixed the Master Storage and Guild Storage window titles not displaying correctly under certain conditions.
- **Mobs**: Fixed a missing tombstone for Phreeoni in `moc_fild15`.
- **Items**: Fixed the appearance of Hunter Bow [1]. originsro/originsro#1434
- **Items**: Updated the description of Assassin Dagger to match the current episode. originsro/originsro#1756
- **Items**: Fixed the description of Sunflower Hats (slotted and unslotted) to mention their unrefinability. originsro/originsro#1377
- **Items**: Fixed items from Kiel Hyre quest being incorrectly described as having 1 weight instead of 0.
- **Skills**: Fixed the EDP status icon tooltip.
- **Skills**: Fixed the SP cost in the Berserk skill's description. originsro/originsro#1714
- **Skills**: Fixed the Spiritual Cadence skill's description. originsro/originsro#1670
- **UI**: Fixed the `@navshop` shop bubble highlight to persist after a teleport.

#### Removed

- **Events**: Removed the Chef NPC from the Halloween event.
- **Events**: Removed the Christmas event NPCs and Antonio spawns.
- **NPCs**: Removed the Spiritual Potion Creation scrolls from the Alchemist Guildsman seller NPC. The scrolls can still be used (until the next update), or sold to any NPC for their original price (assuming Overcharge is used).

This update contains 290 commits.

# Update Changelog - 2020-03-02 (part of Ep. 10.2 - supplemental update)

- **Skills**: Fixed a minor calculation error in the monk combo chaining.
- **NPCs**: Fixed some minor errors in the Katfra dialogues.
- **NPCs**: Fixed an issue in the Utan Shaman NPC, getting stuck on a blank dialogue. originsro/originsro#1821
- **UI**: Fixed an useless and incorrect message displayed on every map change.
- **Skills**: Fixed the Lord Knight Concentration timer icon being reset on teleport.
- **Skills**: Fixed some visual glitches when characters with Assumptio or Kaite use hiding skills.
- **Skills**: Fixed an error in the duration of the visual effect of the Power Up mob skill.
- **Maps**: Added new no-vending areas in Payon, Archer Village and Aldebaran.
- **Maps**: Fixed some references to the old name of Morocc, Morroc.
- **Skills**: Fixed an error in the Skill Tree window preventing allocation of points in the Solar, Lunar and Stellar Union skill. originsro/originsro#1828
- **Skills**: Fixed the requirements of the Super Novice Spirit skill in the Skill Tree window. originsro/originsro#1831
- **Skills**: Updated the description of the Claymore Trap, Land Mine and Blast Mine skills to match the updated formulas. originsro/originsro#1865

This update contains 24 commits.
